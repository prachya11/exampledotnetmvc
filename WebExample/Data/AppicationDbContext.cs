﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebExample.Models;

namespace WebExample.Data
{
    public class AppicationDbContext : DbContext
    {
        public AppicationDbContext(DbContextOptions<AppicationDbContext> options)
      : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Address> Addresse { get; set; }
        public DbSet<Province> Provinces { get; set; }
        public DbSet<Khet> Khets { get; set; }
        public DbSet<Khwang> Khwangs { get; set; }
    }
}
