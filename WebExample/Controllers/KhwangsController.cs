﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebExample.Data;
using WebExample.Models;

namespace WebExample.Controllers
{
    public class KhwangsController : Controller
    {
        private readonly AppicationDbContext _context;

        public KhwangsController(AppicationDbContext context)
        {
            _context = context;
        }

        // GET: Khwangs
        public async Task<IActionResult> Index()
        {
            return View(await _context.Khwangs.ToListAsync());
        }

        // GET: Khwangs/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var khwang = await _context.Khwangs
                .FirstOrDefaultAsync(m => m.Id == id);
            if (khwang == null)
            {
                return NotFound();
            }

            return View(khwang);
        }

        // GET: Khwangs/Create
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,ProvinceID,KhetID,KhwangID,KhwangName,Zipcode")] Khwang khwang)
        {
            if (ModelState.IsValid)
            {
                _context.Add(khwang);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(khwang);
        }

        // GET: Khwangs/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var khwang = await _context.Khwangs.FindAsync(id);
            if (khwang == null)
            {
                return NotFound();
            }
            return View(khwang);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ProvinceID,KhetID,KhwangID,KhwangName,Zipcode")] Khwang khwang)
        {
            if (id != khwang.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(khwang);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!KhwangExists(khwang.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(khwang);
        }

        // GET: Khwangs/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var khwang = await _context.Khwangs
                .FirstOrDefaultAsync(m => m.Id == id);
            if (khwang == null)
            {
                return NotFound();
            }

            return View(khwang);
        }

        // POST: Khwangs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var khwang = await _context.Khwangs.FindAsync(id);
            _context.Khwangs.Remove(khwang);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool KhwangExists(int id)
        {
            return _context.Khwangs.Any(e => e.Id == id);
        }
    }
}
