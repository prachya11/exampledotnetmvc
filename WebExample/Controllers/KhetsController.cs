﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebExample.Data;
using WebExample.Models;

namespace WebExample.Controllers
{
    public class KhetsController : Controller
    {
        private readonly AppicationDbContext _context;

        public KhetsController(AppicationDbContext context)
        {
            _context = context;
        }

        // GET: Khets
        public async Task<IActionResult> Index()
        {
            return View(await _context.Khets.ToListAsync());
        }

        // GET: Khets/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var khet = await _context.Khets
                .FirstOrDefaultAsync(m => m.Id == id);
            if (khet == null)
            {
                return NotFound();
            }

            return View(khet);
        }

        // GET: Khets/Create
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,ProvinceID,KhetID,KhetName")] Khet khet)
        {
            if (ModelState.IsValid)
            {
                _context.Add(khet);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(khet);
        }

        // GET: Khets/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var khet = await _context.Khets.FindAsync(id);
            if (khet == null)
            {
                return NotFound();
            }
            return View(khet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ProvinceID,KhetID,KhetName")] Khet khet)
        {
            if (id != khet.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(khet);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!KhetExists(khet.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(khet);
        }

        // GET: Khets/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var khet = await _context.Khets
                .FirstOrDefaultAsync(m => m.Id == id);
            if (khet == null)
            {
                return NotFound();
            }

            return View(khet);
        }

        // POST: Khets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var khet = await _context.Khets.FindAsync(id);
            _context.Khets.Remove(khet);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool KhetExists(int id)
        {
            return _context.Khets.Any(e => e.Id == id);
        }
    }
}
