﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebExample.Models
{
    public class Address
    {
        [Key]
        public int Id { get; set; }
        public int UserID { get; set; }
        public int ProvinceID { get; set; }
        public int KhetID { get; set; }
        public int KhwangID { get; set; }
        public int Zipcode { get; set; }
    }
}
